// khai báo thư viện express
const express = require("express")

// khỏi tại appp
const app = express();

// khỏi tại port
const port = 8000;

app.use("/", (req, res, next) =>{
    console.log(new Date());
    next()
}, (req, res, next) =>{
    console.log(new Date());
    next()
})

app.post("/", (req, res, next) => {
    console.log(req.method)
    next()
})

app.get("/", (req, res) => {
    let today = new Date();
    res.json({
        message: `Hom nay là ngày ${today.getDate()} tháng ${today.getMonth()} năm ${today.getFullYear()}`
    })
})

app.listen(port, () =>{
    console.log(`Đang chay trên cổng:  ${port}`)
})